import React, { useState } from 'react';
import { DatePicker, message, Alert } from 'antd';
import './index.css';

const AntdDatePicker = () => {
  const [date, setDate] = useState(null);
  const [messageApi, contextHolder] = message.useMessage();
  const handleChange = (value) => {
    messageApi.info(`Selected Date: ${value ? value.format('YYYY-MM-DD') : 'None'}`);
    setDate(value);
  };
  return (
    
    <div style={{ width: 400, margin: '10px auto' }}>
     <h1> Ant Design</h1>
      <DatePicker onChange={handleChange} />
      <div style={{ marginTop: 16 }}>
        Selected Date: {date ? date.format('YYYY-MM-DD') : 'None'}
        <Alert message="Selected Date" description={date ? date.format('YYYY-MM-DD') : 'None'} />
      </div>
      {contextHolder}
    </div>
  );
};

export default AntdDatePicker;
