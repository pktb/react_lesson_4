import React, { useState } from 'react';
import { Input, Radio, Space } from 'antd';
const AntdRadioGroup = () => {
  const [value, setValue] = useState(1);
  const onChange = (e) => {
    console.log('radio checked', e.target.value);
    setValue(e.target.value);
  };
  return (
    <div style={{ width: 400, margin: '10px auto' }}>

<Radio.Group onChange={onChange} value={value}>
      <Space direction="vertical">
        <Radio value={1}>Option A</Radio>
        <Radio value={2}>Option B</Radio>
        <Radio value={3}>Option C</Radio>
        <Radio value={4}>
          More...
          {value === 4 ? (
            <Input
              style={{
                width: 100,
                marginLeft: 10,
              }}
            />
          ) : null}
        </Radio>
      </Space>
    </Radio.Group>
    </div>
 
  );
};
export default AntdRadioGroup;